﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Windows;

namespace PortController
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            List<PortInfo> pi = MainWindow.GetOpenPort();
            lisrtview_scaner.ItemsSource = pi;
        }

        private static List<PortInfo> GetOpenPort()
        {
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] tcpEndPoints = properties.GetActiveTcpListeners();
            TcpConnectionInformation[] tcpConnections = properties.GetActiveTcpConnections();

            return tcpConnections.Select(p =>
            {
                return new PortInfo(
                    i: p.LocalEndPoint.Port,
                    local: String.Format("{0}:{1}", p.LocalEndPoint.Address, p.LocalEndPoint.Port),
                    state: GetTranslate(p.State));
            }).ToList();
        }

        private static string GetTranslate(TcpState tcpState)
        {
            switch(tcpState)
            {
                case TcpState.Closed: return "Закрыто";
                case TcpState.Established: return "Открыто";
                default: return "Не определено";
            }
        }

        private void bBlockPort_Click(object sender, RoutedEventArgs e)
        {
            Process p = new Process();
            p.StartInfo.FileName = "netsh.exe";
            p.StartInfo.Arguments = CalculateRule();
            p.StartInfo.Verb = "runas";
            p.Start();
        }

        private string CalculateRule()
        {
            string dir, protocol, localPort, outParam, name;

            switch (cbTypeIO.SelectedIndex)
            {
                case 0:
                    dir = "in";
                    break;
                case 1:
                    dir = "out";
                    break;
                default:
                    dir = "in";
                    break;
            }

            switch (cbProtocol.SelectedIndex)
            {
                case 0:
                    protocol = "TCP";
                    break;
                case 1:
                    protocol = "UDP";
                    break;
                case 2:
                    protocol = "RDP";
                    break;
                default:
                    protocol = "TCP";
                    break;
            }

            localPort = tbPorts.Text;
            name = string.Format(@"PortController_Block_{0}_{1}", dir, protocol);
            outParam = string.Format(@"advfirewall firewall add rule dir={0} action=block protocol={1} localport={2} name={3}", dir, protocol, localPort, name);

            //MessageBox.Show(outParam);

            return outParam;
        }
    }

    class PortInfo
    {
        public int PortNumber { get; set; }
        public string Local { get; set; }
        public string State { get; set; }

        public PortInfo(int i, string local, string state)
        {
            PortNumber = i;
            Local = local;
            State = state;
        }
    }
}
